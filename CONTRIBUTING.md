# How to Contribute

This document has the basic guidelines you should follow to increase the chances of your changes being accepted. It's assumed that

* [Git](https://git-scm.com/) is your version control system.
* you have Git locally installed and properly configured[^1].
* you already have a GitLab account properly setup.

Be sure to understand this content before submitting merge requests.


[^1]: [Pro Git](https://git-scm.com/book/en/v2) is an online reference book that's freely available and easy to follow.


## 0. Find/Suggest Something of Interest

A good place to start is [the issue tracker](https://gitlab.com/ghost-in-the-zsh/rage/issues). Find something that's needed and/or seems interesting to you. If you don't find it, propose it. Whatever the case, discuss the focus and scope of any proposed changes and/or test cases before putting in the effort.

It's important to keep your changes small and limited in scope when getting started. As you become familiar and earn some trust, the chances of larger and more significant changes being accepted will increase.

Any code that gets merged is code that needs to be maintaned, so please be patient/understanding and be ready for your changes to get reviewed, and maybe even returned to you a few times, before they get merged upstream.


### Issues vs. Feature Requests

Go to the [issue tracker](https://gitlab.com/ghost-in-the-zsh/rage/issues). Be sure to

* verify that your issue has not already been reported.
* pick the appropriate template and fill out as much information as you can.
* clearly describe the issue or feature of interest.

You're encouraged to discuss things before getting started. A bias towards fixing issues before adding new features is generally desired.


## 1. Fork the Repository

* Fork the project in GitLab to get the current source tree under your account.
* Get a local clone of your fork using `git clone git@gitlab.com:<your-account-here>/rage.git`.
    * Your local clone is now tracking the `master` branch of your remote, called `origin` by default.


## 2. Make Your Changes

* Create separate topic branches for **logically unrelated** changes.
    * To do this quickly, use `git checkout -b my_branch master`
    * Topic branches should be based on the `master` branch. Do _not_ work on the `master` branch directly.
* Use descriptive branch names (e.g. `purpose/area/my_contribution`). For example,
    * fixing a mesh import crash: `fix/asset-mesh/wavefront-mesh-loader-crash`
    * adding functionality to transforms: `feature/math/transforms-support-xyz`
    * updating contribution guide: `docs/contrib-guide/improve-section-xyz-wording`
* Separate each **logical change** into a separate commit/patch.
    * Use `git add -p` to choose which parts of a file should be staged.
    * Use `git reset` if you `add`-ed something to the staging area by mistake and need to "start over".
        * Do _not_ pass the `--hard` option to `reset`.
* Make sure your commit messages [follow the proper formatting](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).
* Properly [reference](https://docs.gitlab.com/ce/user/markdown.html#special-gitlab-references) commits, issues, and other artifacts in your commit messages if your changes are related.
* Push commits in your local topic branch to your forked repository.
    * To quickly push your topic branch to your `origin`, use `git push --set-upstream origin your-local-branch-name`.


Remember that the Git reference[^1] can answer your questions. Also, be aware that if a patch is too large, you may be asked to break it into a smaller set of patches.


## 3. Test Your Changes

* Add the necessary tests to validate your changes.
    * Unit tests go under the `tests` source folder.
    * Be sure to put them under the appropriate sub-package(s).
* Run _all_ the tests to ensure nothing else was accidentally broken.

Depending on the nature of a change, additional unit tests may be required before a patch set is considered for merging.


## 4. Style-Check Your Changes

Consistency is important. If you're using the [Eclipse](https://eclipse.org/) IDE, then find and import the following files:

* `rage-code-formatter.xml`
* `rage-cleanup-actions.xml`

They should work "as is". If you can't use these files, you're still responsible for keeping the style consistent. Those that have to read (and maintain!) your code will be grateful.

Make sure the rules are enabled for this project.


### (Reasonable) Exceptions to the Rule

Formatters sometimes mess things up and should be disabled in those sections to improve readability. For example, compare

    @Override
    public Matrix4 getProjectionMatrix() {
        return Matrix4f.createPerspectiveMatrix(getFieldOfViewY(), getAspectRatio(), getNearClipDistance(),
                getFarClipDistance());
    }

with

    @Override
    public Matrix4 getProjectionMatrix() {
        // @formatter:off
        return Matrix4f.createPerspectiveMatrix(
            getFieldOfViewY(),
            getAspectRatio(),
            getNearClipDistance(),
            getFarClipDistance()
        );
        // @formatter:on
    }

You should disable the formatter only in cases where auto-formatting ends up making the code more difficult to follow. In short, stay consistent with the surrounding code, be reasonable, and use common sense.


### Use Wild-Card Imports

Organize your imports in order to avoid noise. A single `java.util.*;` is less noisy than dozens of `java.util.ClassNameHere;` lines for each package. (If they were really *that* important, smart editors wouldn't be auto-hiding them all the time.)

To configure [Eclipse](https://eclipse.org/) to automatically use wild-card imports, follow these steps:

1. Go to: `Window → Preferences → Java → Code Style → Organize Imports`
2. Set the option `Number of imports needed for .* (e.g. org.eclipse.*):` to `1`.
3. Set the option `Number of static imports needed for .* (e.g. java.lang.Math.*):` to `1`.


## 5. Respond to Review Comments

Your patch will almost certainly get comments from reviewers on ways in which the patch can be improved. You must respond to those comments; ignoring reviewers is a good way to get ignored in return. Review comments or questions that do not lead to a code change should almost certainly bring about a comment or changelog entry so that the next reviewer better understands what is going on.

Be sure to tell the reviewers what changes you are making and to thank them for their time. Code review is a tiring and time-consuming process, and reviewers sometimes get grumpy -especially when they're running low on caffeine and hot pockets. Even in that case, though, respond politely and address the problems they have pointed out.

Understand that reviewing a piece of code is not a review of you as a person or your character, so don't take it personally or react defensively. It's just code and we all want it to be the best it can be.


## 6. Be Patient

After you have submitted your change, be patient and wait. Life is complicated and everyone's busy; I may not be able to get to your patch right away. Please, allow a reasonable time frame (e.g. at least a week) before re-submitting or trying to ping me.


## 7. Sign Your Work - The Developer's Certificate of Origin

To track authorship of the work, you _must_ include a "sign-off" line in your commit messages.

The sign-off is a simple line at the end of the explanation for the patch, which certifies that you wrote it or otherwise have the right to pass it on as a free/libre software patch. The rules are pretty simple: if you can certify the following[^2]:

> #### Developer's Certificate of Origin 1.1
>
> By making a contribution to this project, I certify that:
>
> (a) The contribution was created in whole or in part by me and I
>     have the right to submit it under the open source license
>     indicated in the file; or
>
> (b) The contribution is based upon previous work that, to the best
>     of my knowledge, is covered under an appropriate open source
>     license and I have the right under that license to submit that
>     work with modifications, whether created in whole or in part
>     by me, under the same open source license (unless I am
>     permitted to submit under a different license), as indicated
>     in the file; or
>
> (c) The contribution was provided directly to me by some other
>     person who certified (a), (b) or (c) and I have not modified
>     it.
>
> (d) I understand and agree that this project and the contribution
>     are public and that a record of the contribution (including all
>     personal information I submit with it, including my sign-off) is
>     maintained indefinitely and may be redistributed consistent with
>     this project or the open source license(s) involved.

then you just add a line saying:

    Signed-off-by: Random J Developer <random.developer@example.org>

using your real name. (No pseudonyms or anonymous contributions will be accepted.) Adding the `-s` option to `git commit` will include the sign-off line automatically.

[^2]: https://developercertificate.org/
