/**
 * Copyright (C) 2016 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.util;

import static org.testng.AssertJUnit.*;

import java.nio.*;

import org.testng.annotations.*;

public class BufferUtilTest {

    @Test
    public void testByteBufferIsDirect() {
        ByteBuffer buff = BufferUtil.directByteBuffer(new byte[] {});

        assertTrue(buff.isDirect());
    }

    @Test
    public void testByteBufferHasCorrectCapacity() {
        final int capacity = 5;
        ByteBuffer buff = BufferUtil.directByteBuffer(capacity);

        assertTrue(buff.capacity() == capacity);
    }

    @Test
    public void testByteBufferMatchesContents() {
        final byte[] values = new byte[] { 1, 2, 3 };
        ByteBuffer buff = BufferUtil.directByteBuffer(values);

        assertEquals(values[0], buff.get(0));
        assertEquals(values[1], buff.get(1));
        assertEquals(values[2], buff.get(2));
        assertTrue(values.length == buff.capacity());
    }

    @Test
    public void testByteBufferNativeOrderMatches() {
        ByteBuffer buff = BufferUtil.directByteBuffer(new byte[] { 1 });

        assertTrue(buff.order().equals(ByteOrder.nativeOrder()));
    }

    @Test
    public void testIntBufferIsDirect() {
        IntBuffer buff = BufferUtil.directIntBuffer(new int[] {});

        assertTrue(buff.isDirect());
    }

    @Test
    public void testIntBufferHasCorrectCapacity() {
        final int capacity = 5;
        IntBuffer buff = BufferUtil.directIntBuffer(capacity);

        assertTrue(buff.capacity() == capacity);
    }

    @Test
    public void testIntBufferMatchesContents() {
        final int[] values = new int[] { 1, 2, 3 };
        IntBuffer buff = BufferUtil.directIntBuffer(values);

        assertEquals(values[0], buff.get(0));
        assertEquals(values[1], buff.get(1));
        assertEquals(values[2], buff.get(2));
        assertTrue(values.length == buff.capacity());
    }

    @Test
    public void testIntBufferNativeOrderMatches() {
        IntBuffer buff = BufferUtil.directIntBuffer(new int[] { 1 });

        assertTrue(buff.order().equals(ByteOrder.nativeOrder()));
    }

    @Test
    public void testFloatBufferIsDirect() {
        FloatBuffer buff = BufferUtil.directFloatBuffer(new float[] {});

        assertTrue(buff.isDirect());
    }

    @Test
    public void testFloatBufferHasCorrectCapacity() {
        final int capacity = 5;
        FloatBuffer buff = BufferUtil.directFloatBuffer(capacity);

        assertTrue(buff.capacity() == capacity);
    }

    @Test
    public void testFloatBufferMatchesContents() {
        final float[] values = new float[] { 1, 2, 3 };
        FloatBuffer buff = BufferUtil.directFloatBuffer(values);

        assertEquals(values[0], buff.get(0));
        assertEquals(values[1], buff.get(1));
        assertEquals(values[2], buff.get(2));
        assertTrue(values.length == buff.capacity());
    }

    @Test
    public void testFloatBufferNativeOrderMatches() {
        FloatBuffer buff = BufferUtil.directFloatBuffer(new float[] { 1 });

        assertTrue(buff.order().equals(ByteOrder.nativeOrder()));
    }

}
