/**
 * Copyright (C) 2016 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.Color;
import java.io.IOException;

import ray.rage.Engine;
import ray.rage.game.*;
import ray.rage.rendersystem.RenderWindow;
import ray.rage.scene.controllers.RotationController;
import ray.rml.Vector3f;

public class UndefinedMeshMaterialTest extends VariableFrameRateGame {

    public UndefinedMeshMaterialTest() {
        super();
    }

    public static void main(String[] args) {
        Game game = new UndefinedMeshMaterialTest();
        try {
            game.startup();
            game.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            game.shutdown();
            game.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        rw.getViewport(0).setCamera(camera);

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode("CameraNode");
        cameraNode.attachObject(camera);
        cameraNode.setLocalPosition(0, 0, 5);
        cameraNode.lookAt(Vector3f.createZeroVector());
    }

    @Override
    protected void setupScene(Engine eng, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(new Color(.01f, .01f, .01f));

        Entity cubeEntity = sm.createEntity("Cube", "cube_nomat.obj");
        SceneNode cubeNode = sm.getRootSceneNode().createChildSceneNode("CubeNode");
        cubeNode.attachObject(cubeEntity);

        Light light = sm.createLight(cubeEntity.getName() + "Light", Light.Type.DIRECTIONAL);
        light.setDiffuse(Color.WHITE);
        SceneNode lightNode = sm.getRootSceneNode().createChildSceneNode(light.getName() + "Node");
        lightNode.attachObject(light);
        lightNode.setLocalPosition(1, .5f, .65f);

        RotationController rc = new RotationController(Vector3f.createFrom(1, 1, 1));
        rc.addNode(cubeNode);
        sm.addController(rc);
    }

    @Override
    protected void update(Engine engine) {}

}
