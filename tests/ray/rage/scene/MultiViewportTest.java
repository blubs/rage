/**
 * Copyright (C) 2016 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.io.*;

import ray.rage.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.Camera.Frustum.*;

public class MultiViewportTest extends VariableFrameRateGame {

    public MultiViewportTest() {
        super();
    }

    public static void main(String[] args) {
        Game game = new MultiViewportTest();
        try {
            game.startup();
            game.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            game.shutdown();
            game.exit();
        }
    }

    @Override
    protected void setupWindowViewports(RenderWindow rw) {
        rw.addKeyListener(this);

        // these help test getViewport
        Viewport topLeft = rw.getViewport(0);
        topLeft.setDimensions(.51f, .01f, .48f, .48f);
        topLeft.setClearColor(Color.RED);

        rw.createViewport(.51f, .51f, .48f, .48f);
        Viewport topRight = rw.getViewport(1);
        topRight.setClearColor(Color.GREEN);

        // these help test addViewport returns value
        Viewport botLeft = rw.createViewport(.01f, .01f, .48f, .48f);
        botLeft.setClearColor(Color.BLUE);

        Viewport botRight = rw.createViewport(.01f, .51f, .48f, .48f);
        botRight.setClearColor(Color.WHITE);
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        SceneNode rootNode = sm.getRootSceneNode();

        Camera cam1 = sm.createCamera("TopLeftCamera", Projection.PERSPECTIVE);
        rw.getViewport(0).setCamera(cam1);
        SceneNode camNode1 = rootNode.createChildSceneNode(cam1.getName() + "Node");
        camNode1.attachObject(cam1);

        Camera cam2 = sm.createCamera("TopRightCamera", Projection.PERSPECTIVE);
        rw.getViewport(1).setCamera(cam2);
        SceneNode camNode2 = rootNode.createChildSceneNode(cam2.getName() + "Node");
        camNode2.attachObject(cam2);

        Camera cam3 = sm.createCamera("BottomLeftCamera", Projection.PERSPECTIVE);
        rw.getViewport(2).setCamera(cam3);
        SceneNode camNode3 = rootNode.createChildSceneNode(cam3.getName() + "Node");
        camNode3.attachObject(cam3);

        Camera cam4 = sm.createCamera("BottomRightCamera", Projection.PERSPECTIVE);
        rw.getViewport(3).setCamera(cam4);
        SceneNode camNode4 = rootNode.createChildSceneNode(cam4.getName() + "Node");
        camNode4.attachObject(cam4);
    }

    @Override
    protected void setupScene(Engine eng, SceneManager sm) throws IOException {}

    @Override
    protected void update(Engine engine) {}

}
