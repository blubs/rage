/**
 * Copyright (C) 2017 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import ray.rage.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.rendersystem.Renderable.*;
import ray.rage.scene.Camera.Frustum.*;
import ray.rage.scene.controllers.*;
import ray.rml.*;

public class PrimitiveRenderingTest extends VariableFrameRateGame {

    private static final String ENTITY_NAME = "Sphere";

    public PrimitiveRenderingTest() {
        super();
    }

    public static void main(String[] args) {
        Game game = new PrimitiveRenderingTest();
        try {
            game.startup();
            game.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            game.shutdown();
            game.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        SceneNode rootNode = sm.getRootSceneNode();
        Camera camera = sm.createCamera("MainCamera", Projection.PERSPECTIVE);
        rw.getViewport(0).setCamera(camera);

        SceneNode cameraNode = rootNode.createChildSceneNode(camera.getName() + "Node");
        cameraNode.attachObject(camera);
    }

    @Override
    protected void setupScene(Engine eng, SceneManager sm) throws IOException {
        Entity sphere = sm.createEntity(ENTITY_NAME, "sphere.obj");
        sphere.setPrimitive(Primitive.POINTS);

        SceneNode sphereNode = sm.getRootSceneNode().createChildSceneNode(sphere.getName() + "Node");
        sphereNode.moveForward(2.5f);
        sphereNode.attachObject(sphere);

        sm.getAmbientLight().setIntensity(Color.WHITE);

        RotationController rc = new RotationController(Vector3f.createUnitVectorY(), .02f);
        rc.addNode(sphereNode);
        sm.addController(rc);
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    public void keyPressed(KeyEvent e) {
        Entity sphere = getEngine().getSceneManager().getEntity(ENTITY_NAME);
        switch (e.getKeyCode()) {
            case KeyEvent.VK_L:
                sphere.setPrimitive(Primitive.LINES);
                break;
            case KeyEvent.VK_T:
                sphere.setPrimitive(Primitive.TRIANGLES);
                break;
            case KeyEvent.VK_P:
                sphere.setPrimitive(Primitive.POINTS);
                break;
        }
        super.keyPressed(e);
    }

}
