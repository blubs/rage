/**
 * Copyright (C) 2017 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import ray.rage.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.controllers.*;
import ray.rml.*;

public class MultiCameraSceneTest extends VariableFrameRateGame {

    private final static String CAMERA_TL     = "TopLeftCamera";
    private final static String CAMERA_TR     = "TopRightCamera";
    private final static String CAMERA_BL     = "BottomLeftCamera";
    private final static String CAMERA_BR     = "BottomRightCamera";

    private final static String ENTITY_NAME   = "Asteroid";
    private final static String MESH_NAME     = "asteroid_3.obj";

    private final static float  MOVE_OFFSET   = 0.75f;
    private final Vector3       scaleUp       = Vector3f.createFrom(1.1f, 1.1f, 1.1f);
    private final Vector3       scaleDown     = Vector3f.createFrom(.9f, .9f, .9f);

    private SceneNode           activeCameraNode;
    private SceneNode           activeEntityNode;
    private Color               inactiveColor = Color.GRAY;

    public MultiCameraSceneTest() {
        super();
    }

    public static void main(String[] args) {
        Game game = new MultiCameraSceneTest();
        try {
            game.startup();
            game.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            game.shutdown();
            game.exit();
        }
    }

    @Override
    protected void setupWindowViewports(RenderWindow rw) {
        rw.addKeyListener(this);

        Viewport vp = rw.getViewport(0);
        vp.setDimensions(.5f, 0f, .5f, .5f);
        vp.setClearColor(Color.GREEN);

        rw.createViewport(.5f, .5f, .5f, .5f);
        vp = rw.getViewport(1);
        vp.setClearColor(Color.GRAY.darker());

        vp = rw.createViewport(0, 0, .5f, .5f);
        vp.setClearColor(Color.GRAY.darker().darker());

        vp = rw.createViewport(0, .5f, .5f, .5f);
        vp.setClearColor(Color.GRAY.darker().darker().darker());
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera cam1 = sm.createCamera(CAMERA_TL, Camera.Frustum.Projection.PERSPECTIVE);
        cam1.getFrustum().setNearClipDistance(0.1f);
        rw.getViewport(0).setCamera(cam1);
        SceneNode camNode1 = sm.getRootSceneNode().createChildSceneNode(CAMERA_TL + "Node");
        camNode1.setLocalPosition(0, 0, 4);
        camNode1.attachObject(cam1);

        Camera cam2 = sm.createCamera(CAMERA_TR, Camera.Frustum.Projection.PERSPECTIVE);
        cam2.getFrustum().setNearClipDistance(0.1f);
        rw.getViewport(1).setCamera(cam2);
        SceneNode camNode2 = sm.getRootSceneNode().createChildSceneNode(CAMERA_TR + "Node");
        camNode2.setLocalPosition(3.5f, 1f, 4f);
        camNode2.lookAt(camNode2.getLocalPosition().negate());
        camNode2.attachObject(cam2);

        Camera cam3 = sm.createCamera(CAMERA_BL, Camera.Frustum.Projection.PERSPECTIVE);
        cam3.getFrustum().setNearClipDistance(0.1f);
        rw.getViewport(2).setCamera(cam3);
        SceneNode camNode3 = sm.getRootSceneNode().createChildSceneNode(CAMERA_BL + "Node");
        camNode3.setLocalPosition(-3.5f, 1f, -4f);
        camNode3.lookAt(camNode3.getLocalPosition().negate());
        camNode3.attachObject(cam3);

        Camera cam4 = sm.createCamera(CAMERA_BR, Camera.Frustum.Projection.PERSPECTIVE);
        cam4.getFrustum().setNearClipDistance(0.1f);
        rw.getViewport(3).setCamera(cam4);
        SceneNode camNode4 = sm.getRootSceneNode().createChildSceneNode(CAMERA_BR + "Node");
        camNode4.setLocalPosition(1f, -1f, -4f);
        camNode4.lookAt(camNode4.getLocalPosition().negate());
        camNode4.attachObject(cam4);

        activeCameraNode = camNode1;
    }

    @Override
    protected void setupScene(Engine eng, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(Color.WHITE);

        Entity entity = sm.createEntity(ENTITY_NAME, MESH_NAME);

        SceneNode rootNode = sm.getRootSceneNode();
        SceneNode entityNode = rootNode.createChildSceneNode(entity.getName() + "Node");

        entityNode.setLocalScale(.75f, .75f, .75f);
        entityNode.attachObject(entity);
        activeEntityNode = entityNode;

        RotationController rc = new RotationController(Vector3f.createUnitVectorY());
        rc.addNode(activeEntityNode);
        sm.addController(rc);
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
                activeCameraNode.moveForward(MOVE_OFFSET);
                break;
            case KeyEvent.VK_A:
                activeCameraNode.moveLeft(MOVE_OFFSET);
                break;
            case KeyEvent.VK_S:
                activeCameraNode.moveBackward(MOVE_OFFSET);
                break;
            case KeyEvent.VK_D:
                activeCameraNode.moveRight(MOVE_OFFSET);
                break;
            case KeyEvent.VK_L:
                activeCameraNode.lookAt(activeEntityNode);
                break;
            case KeyEvent.VK_F1:
                switchCameras(getEngine().getSceneManager().getCamera(CAMERA_TL));
                break;
            case KeyEvent.VK_F2:
                switchCameras(getEngine().getSceneManager().getCamera(CAMERA_TR));
                break;
            case KeyEvent.VK_F3:
                switchCameras(getEngine().getSceneManager().getCamera(CAMERA_BL));
                break;
            case KeyEvent.VK_F4:
                switchCameras(getEngine().getSceneManager().getCamera(CAMERA_BR));
                break;
            case KeyEvent.VK_ADD:
                activeEntityNode.scale(scaleUp);
                break;
            case KeyEvent.VK_SUBTRACT:
                activeEntityNode.scale(scaleDown);
                break;
        }
        super.keyPressed(e);
    }

    private void switchCameras(Camera newCam) {
        Camera oldCam = (Camera) activeCameraNode.getAttachedObject(0);
        oldCam.getViewport().setClearColor(inactiveColor);

        oldCam = newCam;
        inactiveColor = oldCam.getViewport().getClearColor();
        oldCam.getViewport().setClearColor(Color.GREEN);

        activeCameraNode = newCam.getParentSceneNode();
    }

}
