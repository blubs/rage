/**
 * Copyright (C) 2016 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene.controllers;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import ray.rage.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.rendersystem.states.*;
import ray.rage.rendersystem.states.RenderState.*;
import ray.rage.scene.*;
import ray.rml.*;

public final class RotationControllerTest extends VariableFrameRateGame {

    private static final float ROTATION_SPEED_DELTA = 0.1f;

    private RotationController rotationController   = new RotationController();

    public static void main(String[] args) {
        Game test = new RotationControllerTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        rw.getViewport(0).setCamera(camera);

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode("CameraNode");
        cameraNode.attachObject(camera);
        cameraNode.moveBackward(8);
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(Color.LIGHT_GRAY);

        Entity sphere = sm.createEntity("Sphere", "sphere.obj");
        Entity cube = sm.createEntity("Cube", "cube.obj");

        // set a non-default texture so that the rotation is clearly visible
        Texture tex = sm.getTextureManager().getAssetByPath("red.jpeg");
        ((TextureState) sphere.getSubEntity(0).getRenderState(Type.TEXTURE)).setTexture(tex);

        SceneNode rootNode = sm.getRootSceneNode();
        SceneNode sphereNode = rootNode.createChildSceneNode(sphere.getName() + "Node");
        SceneNode cubeNode = rootNode.createChildSceneNode(cube.getName() + "Node");

        sphereNode.attachObject(sphere);
        cubeNode.attachObject(cube);

        sphereNode.moveRight(3);
        cubeNode.moveLeft(3);

        rotationController.addNode(sphereNode);
        rotationController.addNode(cubeNode);

        sm.addController(rotationController);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_1:
                rotationController.setRotationAxis(Vector3f.createUnitVectorX());
                break;
            case KeyEvent.VK_2:
                rotationController.setRotationAxis(Vector3f.createUnitVectorY());
                break;
            case KeyEvent.VK_3:
                rotationController.setRotationAxis(Vector3f.createUnitVectorZ());
                break;
            case KeyEvent.VK_4:
                rotationController.setRotationAxis(Vector3f.createFrom(1, 1, 1));
                break;
        }
        super.keyPressed(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() < 0)
            rotationController.setSpeed(rotationController.getSpeed() + ROTATION_SPEED_DELTA);
        else
            rotationController.setSpeed(rotationController.getSpeed() - ROTATION_SPEED_DELTA);

        super.mouseWheelMoved(e);
    }

}
