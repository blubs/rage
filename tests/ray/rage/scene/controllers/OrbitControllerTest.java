/**
 * Copyright (C) 2016 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene.controllers;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import ray.rage.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.*;

public final class OrbitControllerTest extends VariableFrameRateGame {

    private static final int      SPHERE_COUNT         = 5;

    private static final float    ORBIT_SPEED_DELTA    = 0.1f;
    private static final float    ORBIT_DISTANCE_DELTA = 0.2f;

    private OrbitController       orbitController;
    private TranslationController translationController;

    public static void main(String[] args) {
        Game test = new OrbitControllerTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        rw.getViewport(0).setCamera(camera);

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode("CameraNode");
        cameraNode.attachObject(camera);
        cameraNode.moveBackward(8f);
        cameraNode.moveUp(2.3f);
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(Color.LIGHT_GRAY);

        Entity cube = sm.createEntity("Cube", "cube.obj");

        SceneNode cubeNode = sm.getRootSceneNode().createChildSceneNode(cube.getName() + "Node");
        cubeNode.attachObject(cube);
        cubeNode.moveForward(2);
        cubeNode.moveLeft(2);

        translationController = new TranslationController();
        translationController.setSpeed(1.5e-3f);
        translationController.setPeriodLengthMillis(3000);
        translationController.addNode(cubeNode);

        orbitController = new OrbitController(cubeNode);
        orbitController.setSpeed(2f);
        orbitController.setDistanceFromTarget(3);
        orbitController.setVerticalDistance(1.25f);

        final float scale = 1f / SPHERE_COUNT + .15f;
        for (int i = 0; i < SPHERE_COUNT; ++i) {
            Entity sphere = sm.createEntity("Sphere" + i, "sphere.obj");

            SceneNode sphereNode = sm.getRootSceneNode().createChildSceneNode(sphere.getName() + "Node");
            sphereNode.scale(scale, scale, scale);
            sphereNode.attachObject(sphere);

            orbitController.addNode(sphereNode);
        }

        sm.addController(translationController);
        sm.addController(orbitController);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_SPACE:
                orbitController.setSpeed(-orbitController.getSpeed());
            case KeyEvent.VK_ADD:
                orbitController.setSpeed(orbitController.getSpeed() + ORBIT_SPEED_DELTA);
                break;
            case KeyEvent.VK_SUBTRACT:
                orbitController.setSpeed(orbitController.getSpeed() - ORBIT_SPEED_DELTA);
                break;
        }
        super.keyPressed(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() < 0)
            orbitController.setVerticalDistance(orbitController.getVerticalDistance() + ORBIT_DISTANCE_DELTA);
        else
            orbitController.setVerticalDistance(orbitController.getVerticalDistance() - ORBIT_DISTANCE_DELTA);

        super.mouseWheelMoved(e);
    }

}
