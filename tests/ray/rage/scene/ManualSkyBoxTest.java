/**
 * Copyright (C) 2016 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.*;

import ray.rage.*;
import ray.rage.asset.material.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.rendersystem.Renderable.*;
import ray.rage.rendersystem.shader.*;
import ray.rage.rendersystem.states.*;
import ray.rage.rendersystem.states.RenderState.*;
import ray.rage.scene.Node.*;
import ray.rage.scene.controllers.*;
import ray.rage.util.*;
import ray.rml.*;

/**
 * This test uses a {@link ManualObject} as the basis for a 'skybox', but seen
 * from the outside. This is a proof of concept for refactoring current skybox
 * code to use this approach.
 *
 * @author Raymond L. Rivera
 *
 */
public class ManualSkyBoxTest extends VariableFrameRateGame {

    private final Angle rotationAngle = Degreef.createFrom(5.5f);
    private boolean     autoRotate    = true;

    public ManualSkyBoxTest() {
        super();
    }

    public static void main(String[] args) {
        Game game = new ManualSkyBoxTest();
        try {
            game.startup();
            game.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            game.shutdown();
            game.exit();
        }
    }

    @Override
    protected void loadConfiguration(Configuration config) throws IOException {
        super.loadConfiguration(config);
        config.setKeyValuePair("assets.skyboxes.path", "assets/skyboxes/test/");
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        rw.getViewport(0).setCamera(camera);

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode("CameraNode");
        cameraNode.attachObject(camera);
        cameraNode.setLocalPosition(-2, 2, 5);
        cameraNode.lookAt(cameraNode.getLocalPosition().negate());
    }

    @Override
    protected void setupScene(Engine eng, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(new Color(.025f, .025f, .025f));

        Configuration conf = eng.getConfiguration();
        eng.getTextureManager().setBaseDirectoryPath(conf.valueOf("assets.skyboxes.path"));

        ManualObject box = createManualObject(eng, sm);
        SceneNode boxNode = sm.getRootSceneNode().createChildSceneNode(box.getName() + "Node");
        boxNode.attachObject(box);

        Light light = sm.createLight(box.getName() + "Light", Light.Type.DIRECTIONAL);
        light.setDiffuse(Color.WHITE);
        SceneNode lightNode = sm.getRootSceneNode().createChildSceneNode(light.getName() + "Node");
        lightNode.attachObject(light);
        lightNode.setLocalPosition(1, .5f, 2);

        RotationController rc = new RotationController(Vector3f.createFrom(1, 1, 1));
        rc.addNode(boxNode);
        sm.addController(rc);
    }

    private ManualObject createManualObject(Engine eng, SceneManager sm) throws IOException {
        TextureManager tm = eng.getTextureManager();
        RenderSystem rs = sm.getRenderSystem();
        ManualObject box = sm.createManualObject("Box");

        // while the ManualObject could've been made of a single
        // ManualObjectSection with single (and smaller)
        // vertex/texcoords/normals buffers, doing it this way allows for
        // a better test case
        ManualObjectSection front = box.createManualSection("Front");
        ManualObjectSection back = box.createManualSection("Back");
        ManualObjectSection left = box.createManualSection("Left");
        ManualObjectSection right = box.createManualSection("Right");
        ManualObjectSection top = box.createManualSection("Top");
        ManualObjectSection bottom = box.createManualSection("Bottom");

        box.setGpuShaderProgram(sm.getRenderSystem().getGpuShaderProgram(GpuShaderProgram.Type.RENDERING));

        // assign specific vertices to each face of the cube
        front.setVertexBuffer(getVerticesFront());
        back.setVertexBuffer(getVerticesBack());
        left.setVertexBuffer(getVerticesLeft());
        right.setVertexBuffer(getVerticesRight());
        top.setVertexBuffer(getVerticesTop());
        bottom.setVertexBuffer(getVerticesBottom());

        // set normal vectors for each face, pointing 'out' of the face
        front.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 0, 0, 1 }));
        back.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 0, 0, -1 }));
        left.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { -1, 0, 0 }));
        right.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 1, 0, 0 }));
        top.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 0, 1, 0 }));
        bottom.setNormalsBuffer(BufferUtil.directFloatBuffer(new float[] { 0, -1, 0 }));

        Texture frontTex = tm.getAssetByPath("front.png");
        Texture backTex = tm.getAssetByPath("back.png");
        Texture leftTex = tm.getAssetByPath("left.png");
        Texture rightTex = tm.getAssetByPath("right.png");
        Texture topTex = tm.getAssetByPath("top.png");
        Texture botTex = tm.getAssetByPath("bottom.png");

        TextureState frontState = (TextureState) rs.createRenderState(Type.TEXTURE);
        TextureState backState = (TextureState) rs.createRenderState(Type.TEXTURE);
        TextureState leftState = (TextureState) rs.createRenderState(Type.TEXTURE);
        TextureState rightState = (TextureState) rs.createRenderState(Type.TEXTURE);
        TextureState topState = (TextureState) rs.createRenderState(Type.TEXTURE);
        TextureState botState = (TextureState) rs.createRenderState(Type.TEXTURE);

        frontState.setTexture(frontTex);
        backState.setTexture(backTex);
        leftState.setTexture(leftTex);
        rightState.setTexture(rightTex);
        topState.setTexture(topTex);
        botState.setTexture(botTex);

        front.setRenderState(frontState);
        back.setRenderState(backState);
        left.setRenderState(leftState);
        right.setRenderState(rightState);
        top.setRenderState(topState);
        bottom.setRenderState(botState);

        FrontFaceState faceState = (FrontFaceState) sm.getRenderSystem().createRenderState(Type.FRONT_FACE);
        box.setRenderState(faceState);

        // use the same texture coords, indices, and material for all sections
        Material mat = eng.getMaterialManager().createManualAsset(box.getName() + "Material");
        box.setTextureCoordBuffer(getTexCoords());
        box.setIndexBuffer(getIndices());
        box.setMaterial(mat);
        box.setDataSource(DataSource.INDEX_BUFFER);

        return box;
    }

    @Override
    protected void update(Engine engine) {
        for (Controller ctrl : engine.getSceneManager().getControllers())
            ctrl.setEnabled(autoRotate);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        SceneManager sm = getEngine().getSceneManager();
        SceneNode boxNode = sm.getSceneNode("BoxNode");

        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                if (boxNode != null)
                    boxNode.yaw(rotationAngle.mult(-1));
                break;
            case KeyEvent.VK_RIGHT:
                if (boxNode != null)
                    boxNode.yaw(rotationAngle);
                break;
            case KeyEvent.VK_UP:
                if (boxNode != null)
                    boxNode.pitch(rotationAngle.mult(-1));
                break;
            case KeyEvent.VK_DOWN:
                if (boxNode != null)
                    boxNode.pitch(rotationAngle);
                break;
            case KeyEvent.VK_SPACE:
                autoRotate = !autoRotate;
                break;
        }
        super.keyPressed(e);
    }

    private static FloatBuffer getVerticesFront() {
        // @formatter:off
        float[] vertices = new float[] {
            -1f,  1f, 1f,
            -1f, -1f, 1f,
             1f, -1f, 1f,
             1f,  1f, 1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesBack() {
        // @formatter:off
        float[] vertices = new float[] {
             1f,  1f, -1f,
             1f, -1f, -1f,
            -1f, -1f, -1f,
            -1f,  1f, -1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesLeft() {
        // @formatter:off
        float[] vertices = new float[] {
            -1f,  1f, -1f,
            -1f, -1f, -1f,
            -1f, -1f,  1f,
            -1f,  1f,  1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesRight() {
        // @formatter:off
        float[] vertices = new float[] {
            1f,  1f,  1f,
            1f, -1f,  1f,
            1f, -1f, -1f,
            1f,  1f, -1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesTop() {
        // @formatter:off
        float[] vertices = new float[] {
            -1f,  1f, -1f,
            -1f,  1f,  1f,
             1f,  1f,  1f,
             1f,  1f, -1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getVerticesBottom() {
        // @formatter:off
        float[] vertices = new float[] {
            -1f, -1f,  1f,
            -1f, -1f, -1f,
             1f, -1f, -1f,
             1f, -1f,  1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static FloatBuffer getTexCoords() {
        // @formatter:off
        float[] texcoords = new float[] {
            0, 1,
            0, 0,
            1, 0,
            1, 1
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(texcoords);
    }

    private static IntBuffer getIndices() {
        // @formatter:off
        int[] indices = new int[] {
            0, 1, 2,
            0, 2, 3
        };
        // @formatter:on
        return BufferUtil.directIntBuffer(indices);
    }

}
