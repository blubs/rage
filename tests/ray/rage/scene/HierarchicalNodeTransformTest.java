/**
 * Copyright (C) 2017 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.io.*;

import ray.rage.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.rendersystem.states.*;
import ray.rage.scene.Camera.Frustum.*;
import ray.rage.scene.controllers.*;
import ray.rml.*;

public class HierarchicalNodeTransformTest extends VariableFrameRateGame {

    public HierarchicalNodeTransformTest() {
        super();
    }

    public static void main(String[] args) {
        Game game = new HierarchicalNodeTransformTest();
        try {
            game.startup();
            game.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            game.shutdown();
            game.exit();
        }
    }

    @Override
    protected void setupWindowViewports(RenderWindow rw) {
        rw.addKeyListener(this);

        Viewport vp = rw.getViewport(0);
        vp.setClearColor(Color.GRAY);
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        SceneNode rootNode = sm.getRootSceneNode();
        Camera camera = sm.createCamera("MainCamera", Projection.PERSPECTIVE);
        rw.getViewport(0).setCamera(camera);

        SceneNode cameraNode = rootNode.createChildSceneNode(camera.getName() + "Node");
        cameraNode.attachObject(camera);
    }

    @Override
    protected void setupScene(Engine eng, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(Color.WHITE);

        TextureManager tm = eng.getTextureManager();
        Texture redTexture = tm.getAssetByPath("red.jpeg");
        Texture blueTexture = tm.getAssetByPath("blue.jpeg");

        final String meshPath = "sphere.obj";
        Entity oxygenAtom = sm.createEntity("Oxygen", meshPath);
        Entity hydrogenAtom1 = sm.createEntity("Hydrogen1", meshPath);
        Entity hydrogenAtom2 = sm.createEntity("Hydrogen2", meshPath);

        RenderSystem rs = sm.getRenderSystem();
        TextureState redState = (TextureState) rs.createRenderState(RenderState.Type.TEXTURE);
        TextureState blueState = (TextureState) rs.createRenderState(RenderState.Type.TEXTURE);

        redState.setTexture(redTexture);
        blueState.setTexture(blueTexture);

        oxygenAtom.setRenderState(redState);
        hydrogenAtom1.setRenderState(blueState);
        hydrogenAtom2.setRenderState(blueState);

        SceneNode rootNode = sm.getRootSceneNode();
        SceneNode oxygenNode = rootNode.createChildSceneNode(oxygenAtom.getName() + "Node");
        SceneNode hydrogenNode1 = oxygenNode.createChildSceneNode(hydrogenAtom1.getName() + "Node");
        SceneNode hydrogenNode2 = oxygenNode.createChildSceneNode(hydrogenAtom2.getName() + "Node");

        oxygenNode.moveForward(5);
        oxygenNode.moveUp(.4f);
        oxygenNode.setLocalScale(.75f, .75f, .75f);
        hydrogenNode1.scale(.5f, .5f, .5f);
        hydrogenNode2.scale(hydrogenNode1.getLocalScale());

        oxygenNode.attachObject(oxygenAtom);
        hydrogenNode1.attachObject(hydrogenAtom1);
        hydrogenNode2.attachObject(hydrogenAtom2);

        hydrogenNode1.translate(-2f, -1f, 0);
        hydrogenNode2.translate(2f, -1f, 0);

        RotationController rc = new RotationController(Vector3f.createUnitVectorY());
        rc.addNode(oxygenNode);
        sm.addController(rc);
    }

    @Override
    protected void update(Engine engine) {}

}
