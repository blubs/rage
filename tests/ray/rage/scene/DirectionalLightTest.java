/**
 * Copyright (C) 2016 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.Color;
import java.awt.event.*;
import java.io.IOException;

import ray.rage.Engine;
import ray.rage.game.*;
import ray.rage.rendersystem.RenderWindow;
import ray.rage.scene.controllers.OrbitController;
import ray.rml.Vector3f;

public class DirectionalLightTest extends VariableFrameRateGame {

    private final static String LIGHT_NAME       = "LightSource";
    private Color               ambientIntensity = new Color(.02f, .02f, .02f);

    public DirectionalLightTest() {
        super();
    }

    public static void main(String[] args) {
        Game test = new DirectionalLightTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        rw.getViewport(0).setCamera(camera);

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode(camera.getName() + "Node");
        cameraNode.attachObject(camera);

        // place camera at location where problems have been spotted before
        cameraNode.moveForward(8);
        cameraNode.lookAt(Vector3f.createZeroVector());
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        SceneNode rootNode = sm.getRootSceneNode();

        Entity target = sm.createEntity("Target", "sphere.obj");
        SceneNode targetNode = rootNode.createChildSceneNode(target.getName() + "Node");
        targetNode.attachObject(target);

        // the debug entity lets you see the actual direction from which light
        // is supposed to be coming from; do not confuse this with point lights
        Entity debugEntity = sm.createEntity("DebugLightEntity", "sphere.obj");

        Light light = sm.createLight(LIGHT_NAME, Light.Type.DIRECTIONAL);
        light.setDiffuse(Color.CYAN);
        light.setSpecular(Color.CYAN);
        SceneNode lightNode = rootNode.createChildSceneNode(light.getName() + "Node");
        lightNode.attachObject(light);
        lightNode.attachObject(debugEntity);
        lightNode.scale(.15f, .15f, .15f);

        sm.getAmbientLight().setIntensity(ambientIntensity);

        OrbitController ctrl = new OrbitController(targetNode);
        ctrl.setDistanceFromTarget(5); // close enough to be visible
        ctrl.setVerticalDistance(1);
        ctrl.addNode(lightNode);
        sm.addController(ctrl);
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_R:
                updateLightColor(Color.RED);
                break;
            case KeyEvent.VK_G:
                updateLightColor(Color.GREEN);
                break;
            case KeyEvent.VK_B:
                updateLightColor(Color.BLUE);
                break;
            case KeyEvent.VK_W:
                updateLightColor(Color.WHITE);
                break;
        }
        super.keyPressed(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() < 0)
            ambientIntensity = ambientIntensity.brighter();
        else
            ambientIntensity = ambientIntensity.darker();

        getEngine().getSceneManager().getAmbientLight().setIntensity(ambientIntensity);
    }

    private void updateLightColor(Color c) {
        Light l = getEngine().getSceneManager().getLight(LIGHT_NAME);
        l.setDiffuse(c);
        l.setSpecular(c);
    }

}
