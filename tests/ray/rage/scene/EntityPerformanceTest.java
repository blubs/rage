/**
 * Copyright (C) 2017 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.util.Random;

import ray.rage.Engine;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.RenderWindow;
import ray.rage.scene.controllers.*;
import ray.rage.util.Configuration;
import ray.rml.*;

/**
 * A simple stress test to see how the system performs when handling a large
 * number of objects.
 * <p>
 * For (an anecdotal/unscientific) comparison, I had tried using the same
 * asteroid meshes included here to load 45 asteroid objects into the game world
 * of my CSc-165 project in 2014 (using the original SAGE), but the engine
 * couldn't handle it. It would take a CPU core to 100%, about 1 minute or more
 * to display a screen, consume ~10GB+ of memory, and be unusable due to
 * single-digit FPS counts due to resource managers duplicating the same data in
 * memory every time, the deprecated fixed-function pipeline, and other details.
 * <p>
 * Specs for this machine are:
 * <ul>
 * <li>Intel Core i7 950 @ 3.07GHz</li>
 * <li>12GB RAM</li>
 * <li>NVIDIA GTX-770 Ti 3GB</li>
 * <li>Windows 7 x64</li>
 * </ul>
 * <p>
 * OTOH, this test creates 1,200 Entities, each with its own Controller and
 * other data, using the same asteroid Meshes I had used in the previous test
 * (each Mesh loaded only once here), takes a single core to ~95-100%, displayed
 * the scene in ~8 seconds, consumed ~2.5-3.5GBs memory, and maintained a
 * reasonably playable frame rate on the same machine.
 *
 * @author Raymond L. Rivera
 *
 */
public class EntityPerformanceTest extends VariableFrameRateGame {

    private final static int   MAX_TOTAL_ASTEROIDS        = 1200;
    private final static int   MAX_ASTEROIDS_PER_RING     = 75;
    private final static int   ASTEROID_DISTANCE_PER_RING = 2;
    private final static float ASTEROID_SCALING_FACTOR    = .3f;
    private final static float MIN_ORBITAL_SPEED          = 0.001f;
    private final static float ORBITAL_SPEED_DELTA        = 0.003f;
    private final static float PLANET_SCALING_FACTOR      = 6f;
    private final static int   WAYPOINT_INTERVAL_MS       = 15000;

    private Random             random                     = new Random();

    private SceneNode          cameraNode;
    private SceneNode          planetNode;

    public EntityPerformanceTest() {
        super();
    }

    public static void main(String[] args) {
        Game test = new EntityPerformanceTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        rw.getViewport(0).setCamera(camera);

        cameraNode = sm.getRootSceneNode().createChildSceneNode(camera.getName() + "Node");
        cameraNode.attachObject(camera);

        // @formatter:off
        float[][] waypoints = {
            // x, y, z
            {  50 , 35,  125 },
            {  50 , 12,  100 },
            { -50 , -8,   85 },
            { -115,  2,   -5 },
            { -75 ,  5, -115 },
            {   0 ,  5, -115 },
            { 110 , 12,  -40 },
            {  60 , 25,   60 }
        };
        // @formatter:on

        setupCameraNodeWaypoints(sm, cameraNode, waypoints);

        float[] point = waypoints[0];
        cameraNode.setLocalPosition(point[0], point[1], point[2]);
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        setupSkyBox(engine, sm);

        Entity planet = sm.createEntity("Planet", "earth.obj");
        planetNode = sm.getRootSceneNode().createChildSceneNode(planet.getName() + "Node");
        planetNode.attachObject(planet);
        planetNode.scale(PLANET_SCALING_FACTOR, PLANET_SCALING_FACTOR, PLANET_SCALING_FACTOR);
        planetNode.setLocalPosition(0, 1, 0);
        cameraNode.lookAt(planetNode);
        RotationController planetRotation = new RotationController(Vector3f.createUnitVectorY(), .005f);
        planetRotation.addNode(planetNode);
        sm.addController(planetRotation);

        float orbitalSpeedLevel = 0.075f;
        OrbitController ringOrbitCtrl = new OrbitController(planetNode);
        ringOrbitCtrl.setDistanceFromTarget(75);
        ringOrbitCtrl.setSpeed(orbitalSpeedLevel);
        sm.addController(ringOrbitCtrl);

        int ringLevel = 0;
        for (int i = 0; i < MAX_TOTAL_ASTEROIDS; ++i) {
            // alternate between 3 different asteroid meshes for some variation
            // and increase stress on the system (i.e. more resources)
            Entity rock = sm.createEntity("Asteroid-" + i, "asteroid_" + (i % 3 + 1) + ".obj");
            SceneNode rockNode = sm.getRootSceneNode().createChildSceneNode(rock.getName() + "Node");
            rockNode.attachObject(rock);
            rockNode.scale(ASTEROID_SCALING_FACTOR, ASTEROID_SCALING_FACTOR, ASTEROID_SCALING_FACTOR);
            ringOrbitCtrl.addNode(rockNode);

            // set different rotation speeds/axes for each asteroid
            Vector3 axis = Vector3f.createFrom(random.nextFloat(), random.nextFloat(), random.nextFloat());
            RotationController rockRotation = new RotationController(axis, random.nextFloat());
            rockRotation.addNode(rockNode);
            sm.addController(rockRotation);

            // create a new ring if we've reached the maximum number of
            // asteroids per ring
            if (++ringLevel % MAX_ASTEROIDS_PER_RING == 0) {
                // orbital speed goes down as asteroids move away from the
                // center, but to a minimum
                orbitalSpeedLevel = Math.max(MIN_ORBITAL_SPEED, orbitalSpeedLevel - ORBITAL_SPEED_DELTA);

                float ringDistance = ringOrbitCtrl.getDistanceFromTarget();
                ringOrbitCtrl = new OrbitController(planetNode);
                ringOrbitCtrl.setDistanceFromTarget(ringDistance + ASTEROID_DISTANCE_PER_RING);
                ringOrbitCtrl.setSpeed(orbitalSpeedLevel);

                // slightly alternate the height of their orbits relative to the
                // plane to avoid having all of them perfectly aligned
                ringOrbitCtrl.setVerticalDistance(random.nextFloat() * (ringLevel % 2 == 0 ? 1 : -1));
                sm.addController(ringOrbitCtrl);
            }
        }
        Light star = sm.createLight("Sun", Light.Type.DIRECTIONAL);
        star.setDiffuse(Color.WHITE);
        SceneNode starNode = sm.getRootSceneNode().createChildSceneNode(star.getName() + "Node");
        starNode.attachObject(star);
        starNode.setLocalPosition(1, 0, .5f);
        sm.getAmbientLight().setIntensity(new Color(.025f, .025f, .025f));
    }

    private void setupSkyBox(Engine engine, SceneManager sm) throws IOException {
        Configuration conf = engine.getConfiguration();
        TextureManager textureMgr = engine.getTextureManager();

        textureMgr.setBaseDirectoryPath(conf.valueOf("assets.skyboxes.path"));
        Texture front = textureMgr.getAssetByPath("front.png");
        Texture back = textureMgr.getAssetByPath("back.png");
        Texture left = textureMgr.getAssetByPath("left.png");
        Texture right = textureMgr.getAssetByPath("right.png");
        Texture top = textureMgr.getAssetByPath("top.png");
        Texture bottom = textureMgr.getAssetByPath("bottom.png");
        textureMgr.setBaseDirectoryPath(conf.valueOf("assets.textures.path"));

        // cubemap textures must be flipped up-side-down to face inward; all
        // textures must have the same dimensions, so any image height will do
        AffineTransform xform = new AffineTransform();
        xform.translate(0, front.getImage().getHeight());
        xform.scale(1d, -1d);

        front.transform(xform);
        back.transform(xform);
        left.transform(xform);
        right.transform(xform);
        top.transform(xform);
        bottom.transform(xform);

        SkyBox sb = sm.createSkyBox("SkyBox");
        sb.setTexture(front, SkyBox.Face.FRONT);
        sb.setTexture(back, SkyBox.Face.BACK);
        sb.setTexture(left, SkyBox.Face.LEFT);
        sb.setTexture(right, SkyBox.Face.RIGHT);
        sb.setTexture(top, SkyBox.Face.TOP);
        sb.setTexture(bottom, SkyBox.Face.BOTTOM);
        sm.setActiveSkyBox(sb);
    }

    @Override
    protected void update(Engine engine) {
        reTargetCamera();
    }

    private void reTargetCamera() {
        Vector3 fwd = cameraNode.getWorldForwardAxis();
        cameraNode.lookAt(fwd.x(), planetNode.getWorldPosition().y(), fwd.z());
    }

    private void setupCameraNodeWaypoints(SceneManager sm, SceneNode node, float[][] waypoints) {
        WaypointController ctrl = new WaypointController();
        ctrl.addNode(node);
        ctrl.setIntervalTimeMillis(WAYPOINT_INTERVAL_MS);
        for (float[] values : waypoints)
            ctrl.addWaypoint(Vector3f.createFrom(values));

        sm.addController(ctrl);
    }

}
