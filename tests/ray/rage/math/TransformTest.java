/**
 * Copyright (C) 2017 Raymond L. Rivera <ray.l.rivera@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.math;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

import ray.rml.*;

public class TransformTest {

    @Test
    public void testFactoryMatchesDefault() {
        Transform t = Transform.createDefault();

        assertTrue(t.position().equals(Vector3f.createZeroVector()));
        assertTrue(t.rotation().equals(Matrix3f.createIdentityMatrix()));
        assertTrue(t.scale().equals(Vector3f.createFrom(1, 1, 1)));
    }

    @Test
    public void testFactoryMatchesParam1() {
        Transform t = Transform.createFrom(Vector3f.createUnitVectorX());

        assertTrue(t.position().equals(Vector3f.createUnitVectorX()));
        assertTrue(t.rotation().equals(Matrix3f.createIdentityMatrix()));
        assertTrue(t.scale().equals(Vector3f.createFrom(1, 1, 1)));
    }

    @Test
    public void testFactoryMatchesParam2() {
        Transform t = Transform.createFrom(Vector3f.createUnitVectorX(), Matrix3f.createIdentityMatrix());

        assertTrue(t.position().equals(Vector3f.createUnitVectorX()));
        assertTrue(t.rotation().equals(Matrix3f.createIdentityMatrix()));
        assertTrue(t.scale().equals(Vector3f.createFrom(1, 1, 1)));
    }

    @Test
    public void testFactoryMatchesParam3() {
        // @formatter:off
        Transform t = Transform.createFrom(
            Vector3f.createUnitVectorX(),
            Matrix3f.createIdentityMatrix(),
            Vector3f.createUnitVectorY()
        );
        // @formatter:on

        assertTrue(t.position().equals(Vector3f.createUnitVectorX()));
        assertTrue(t.rotation().equals(Matrix3f.createIdentityMatrix()));
        assertTrue(t.scale().equals(Vector3f.createUnitVectorY()));
    }

    @Test
    public void testFactoryMatchesParamXform() {
        Transform t1 = Transform.createDefault();
        Transform t2 = Transform.createFrom(t1);

        assertTrue(t1.equals(t2));
    }

    @Test
    public void testSetPositionVectorMatches() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;

        Transform t = Transform.createDefault();
        t.setPosition(Vector3f.createFrom(x, y, z));

        assertEquals(t.position().x(), x);
        assertEquals(t.position().y(), y);
        assertEquals(t.position().z(), z);
    }

    @Test
    public void testSetPositionPrimitiveMatches() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;

        Transform t = Transform.createDefault();
        t.setPosition(x, y, z);

        assertEquals(t.position().x(), x);
        assertEquals(t.position().y(), y);
        assertEquals(t.position().z(), z);
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testSetPositionNullRejected() {
        Transform t = Transform.createDefault();

        t.setPosition(null);
    }

    @Test
    public void testSetRotationMatrixMatches() {
        Matrix3 rotation = Matrix3f.createRotationFrom(Degreef.createFrom(90), Vector3f.createUnitVectorY());

        Transform t = Transform.createDefault();
        t.setRotation(rotation);

        assertTrue(t.rotation().equals(rotation));
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testSetRotationMatrixNullRejected() {
        Transform t = Transform.createDefault();

        t.setRotation(null);
    }

    @Test
    public void testSetScalingVectorMatches() {
        final float x = 2f;
        final float y = 4f;
        final float z = 6f;

        Transform t = Transform.createDefault();
        t.setScale(Vector3f.createFrom(x, y, z));

        assertEquals(t.scale().x(), x);
        assertEquals(t.scale().y(), y);
        assertEquals(t.scale().z(), z);
    }

    @Test
    public void testSetScalingPrimitiveMatches() {
        final float x = 2f;
        final float y = 4f;
        final float z = 6f;

        Transform t = Transform.createDefault();
        t.setScale(x, y, z);

        assertEquals(t.scale().x(), x);
        assertEquals(t.scale().y(), y);
        assertEquals(t.scale().z(), z);
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testSetScalingNullRejected() {
        Transform t = Transform.createDefault();

        t.setScale(null);
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Transform t = Transform.createDefault();

        assertTrue(t.equals(t));
        assertFalse(t.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Transform t1 = Transform.createDefault();
        Transform t2 = Transform.createDefault();

        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t1));

        assertTrue(t1.hashCode() == t2.hashCode());

        assertFalse(t1.equals(null));
        assertFalse(t2.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Transform t1 = Transform.createDefault();
        Transform t2 = Transform.createDefault();
        Transform t3 = Transform.createDefault();

        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t3));
        assertTrue(t1.equals(t3));

        assertTrue(t1.hashCode() == t2.hashCode());
        assertTrue(t2.hashCode() == t3.hashCode());
        assertTrue(t1.hashCode() == t3.hashCode());

        assertFalse(t1.equals(null));
        assertFalse(t2.equals(null));
        assertFalse(t3.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Transform t1 = Transform.createDefault();
        Transform t2 = Transform.createDefault();

        assertTrue(t1.equals(t2));
        assertTrue(t1.equals(t2));

        assertTrue(t1.hashCode() == t2.hashCode());
        assertTrue(t1.hashCode() == t2.hashCode());

        t1.setPosition(1, 2, 3);
        assertFalse(t1.equals(t2));
        assertFalse(t1.hashCode() == t2.hashCode());

        assertFalse(t1.equals(null));
        assertFalse(t2.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(Transform.createDefault().equals(null));
    }

}
