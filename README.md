# RAGE

RAGE is a bare bones Java-based game development framework, or "engine", with the objective of serving as a teaching tool that supports the development of interactive 3D games at the undergraduate level. It is ***not*** intended as system for professional game development, even if it can (eventually) support somewhat similar features.

The name can be interpreted in multiple ways:

1. **R**aymond's "**A**wesome"[^1] **G**ame **E**ngine, for simple people
2. **R**AGE repl**A**ces sa**GE**, for recursion enthusiasts
3. Rage, for when things don't work as intended
4. RAGE Engine, for [RAS Syndrome](https://en.wikipedia.org/wiki/RAS_syndrome) victims
5. Totally *unrelated* to [RAGE](https://en.wikipedia.org/wiki/Rockstar_Advanced_Game_Engine)

RAGE includes a scene graph and a modern OpenGL-based renderer implementation built around the programmable shader pipeline[^2].

[^1]: Don't take this too seriously 😐😑

[^2]: Some architectural decisions in RAGE were partly influenced by [OGRE3D](http://www.ogre3d.org/).


## Environment Setup

This project has the following external dependencies:

* [RAGE Math Library (RML)](https://gitlab.com/ghost-in-the-zsh/rml)
* [Java OpenGL Bindings (JOGL)](http://jogamp.org/)
* [TestNG](http://testng.org)

These libraries must be included in your project's classpath.


## Minimum System Requirements

* GNU/Linux, Microsoft Windows 7 or later.
* Java (JDK/JRE) 1.8 or later (64-bit)
* Discrete graphics hardware and latest drivers[^3]
    * GPU must be OpenGL/GLSL 4.3-compliant or better[^4].


Most of the development and testing efforts have been made in a [Kubuntu](https://kubuntu.org/)-based system, a GNU/Linux variant based on the [Ubuntu](https://www.ubuntu.com/) distribution, with Windows-based systems (7 and 10) coming in second. Functionality under macOS is _not_ supported; their implementation is known to be out of date and is sometimes the cause for observed issues[^5].

[^3]: Only NVIDIA GPUs were available during development. Testing on non-NVIDIA GPUs (e.g. AMD, Intel, etc.) is needed and welcomed.

[^4]: This requirement may change based on system features, 3D APIs, etc.

[^5]: "The source no longer supports Mac because the OpenGL implementation is too out of date to run any of the new samples and the overhead of keeping the more basic samples working there is too great." [OpenGL SuperBible](http://www.openglsuperbible.com/example-code/). Accessed April, 2017.


## Documentation

Use your IDE to generate the javadocs. [Doxygen](http://www.stack.nl/~dimitri/doxygen/) can also be used.


## Contributions

See the [contribution guide](CONTRIBUTING.md) for important details. It covers several topics including

* reporting issues;
* requesting features; and
* submitting code changes

among others. Reading the [bug reporting guide](REPORTING-BUGS.md) is also recommended.


## Licensing

This project is [Free Software](https://www.gnu.org/philosophy/free-sw.html) because it respects and protects your freedoms. For a quick summary of what your rights and responsibilities are, you should see [this article](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)). For the full text, you can see the [license](LICENSE.md).
