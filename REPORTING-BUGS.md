# Reporting Issues

Make sure the issue you've encountered has not yet been reported in the [issue tracker](https://gitlab.com/ghost-in-the-zsh/rage/issues). The rest of this document assumes you've already done this. A brief 5 minute search can save you and me a significant amount of time and duplicated effort.


## Intent

This document is intended to be a _guide_ of useful concepts to keep in mind in order to make your submission useful to others.

Note that I have already included simplified report templates in the tracker, which is the preferred reporting method. However, if there's some rare circumstance that requires using email instead, then this document should be used as the starting point.


## Tips for Reporting Bugs

If you haven't reported a bug before, please take a look at the articles below. Familiarizing yourself with the general points and ideas should be enough.

* http://www.chiark.greenend.org.uk/~sgtatham/bugs.html
* http://www.catb.org/esr/faqs/smart-questions.html

It's **REALLY** important to report bugs that seem unrelated as _separate_ issues. If you mix several unrelated issues together, it'll be difficult for maintainers to find the relevant data, and those that come after you might submit duplicates by mistake.


## Gather information

The most important information in a bug report is ***how to reproduce the bug***. This includes system information, and (most importantly) _step-by-step instructions_ for how a user can trigger the bug. If the failure includes a stack trace, be sure to include that as well.


## Relevant Information Checklist

Having a standardized bug report form makes it easier for you not to overlook things, and easier for the developers to find the pieces of information they're really interested in.

This is a _suggested_ checklist for bug report data, especially when reporting via email. Feel free to exclude any information you think is not relevant.

First find the engine's version, ideally a commit hash/tag, where the issue was found. Use that information to fill in all relevant fields of the bug report form, and send/post it with a subject of **"BUG: [One line problem summary here]"** for easy identification and filtering by the developer(s).

1. One line summary of the problem
2. Full description of the problem/report
3. Most recent engine version which did _not_ have the bug (commit hash/tag if possible)
4. A small example program which triggers the problem (if possible)
5. Environment
    1. Operating system information (e.g. name, version, architecture)
    2. Desktop manager/compositing engine (e.g. Explorer, Gnome, KDE) and version information
    3. Java version (e.g. 1.8)
6. Graphics card information (if problem relates to rendering)
    1. Graphics processor (e.g. GeForce GTX 770, GTX 960M, etc):
    2. Color depth bits
    3. Loaded driver version
    4. Total dedicated video memory
    5. Video resolution
    6. Loaded 3D API extensions
    7. Other information that might be relevant to the problem
7. Other notes, patches, fixes, workarounds, etc.

Again, include _relevant_ information only and communicate as clearly as you can.


## Follow up

After your initial report, more information may be necessary. Please, be ready for this.


### Expectations for Bug Reporters

Engine maintainers expect bug reporters to be able to follow up on their reports. That may include

* running new tests;
* applying patches; and/or
* re-triggering your bug.

The most frustrating thing for maintainers is for someone to report a bug, and then never follow up on a request to try out a fix.

That said, it's still useful for a maintainer to know a bug exists on a supported version, even if you can't follow up with retests. Follow up reports, such as replying with

> I tried the latest version and I can't reproduce my bug anymore

are also helpful, because maintainers have to assume silence means things are still broken.


### Expectations for Engine Maintainers

Engine maintainers are busy, overworked human beings[^1]. Some times they may not be able to address your bug in a day, a week, or two weeks. If they don't answer your email, private message, etc. they may be on vacation or, in general, dealing with a different situation.

In general, engine maintainers may take a few days to respond to reported bugs. The majority of engine maintainers might be students involved in course projects to work on the engine, and they may choose not to contiune working in it after a given semester has ended. Unless you have a high priority bug, please wait at least a week after the first bug report before trying to send reminders.

The exceptions to this rule are regressions, engine crashes, or client breakage caused by new engine behavior. Those bugs should be given priority.


[^1]: Currently, that's only me, myself, and Khrono - my trusted bearded dragon.
